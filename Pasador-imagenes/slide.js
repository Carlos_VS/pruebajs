var fondos = ["http://2.bp.blogspot.com/_EZ16vWYvHHg/S-Bl2fuyyWI/AAAAAAAAMKc/DNayYJK8mEo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-20.jpg", "http://www.enbolivia.com/files/imagen_corporativa.jpg", "http://2.bp.blogspot.com/_EZ16vWYvHHg/S-BkIUOuxeI/AAAAAAAAMH0/xJllGDfubHo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-3.jpg", "http://www.imagenes-juegos.com/halo-4-imagen-i306424-i.jpg", "http://img.emol.com/2012/01/25/impresionante-imagen-tierra_14302.jpg"];

var pos = 0;
var valor = 0;
var int = 0;
var timeoutId;



function to_right() {
    $("#background").toggle("slide", {
        direction: 'left',
        duration: 400
    });

    setTimeout(function () {
        change_Img("right");
        $("#background").toggle("slide", {
            direction: 'right'
        });

    }, 200);
}

function to_left() {
    $("#background").toggle("slide", {
        direction: 'right',
        duration: 400
    });

    setTimeout(function () {
        change_Img("left");
        $("#background").toggle("slide", {
            direction: 'left'
        });

    }, 200);
}


function change_Img(direction) {
    //******DERECHA*******
    if (direction === "right") {
        if (pos >= fondos.length - 1) {
            pos = 0;
        } else {
            pos = (pos % fondos.length) + 1;
        }
        valor = 0;
        $("#background").css('background-image', 'url(' + fondos[pos] + ')');
    }

    //*****IZQUIERDA*****
    if (direction === "left") {
        if (pos < 1) {
            pos = fondos.length - 1;
        } else {
            pos = (pos % fondos.length) - 1;
        }
        valor = 0;
        $("#background").css('background-image', 'url(' + fondos[pos] + ')');
    }
}




$(document).ready(function () {
    $(".img-responsive").fadeTo(0, 0.5);

    $("#right").mouseleave(function () {
        $("#right").fadeTo(0, 0.5);
        $("body").css('cursor', 'pointer');

    });

    $("#left").mouseleave(function () {
        $("#left").fadeTo(0, 0.5);
        $("body").css('cursor', 'pointer');

    });



    //IZQUIERDA*************************
    $("#left").hover(function () {
        $("body").css('cursor', 'wait');

        $("#left").fadeTo(0, 1);
        if (!timeoutId) {
            timeoutId = window.setTimeout(function () {
                timeoutId = null;
                to_left();

            }, 1000);
        }

    }, function () {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timeoutId = null;
        }
    });


    //DERECHA*************************
    $("#right").hover(function () {
        $("body").css('cursor', 'wait');

        $("#right").fadeTo(0, 1);
        if (!timeoutId) {
            timeoutId = window.setTimeout(function () {
                timeoutId = null;
                to_right();

            }, 1000);
        }

    }, function () {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timeoutId = null;
        }
    });



    //BARRA PROGRESO********
    $(function mover() {
        // definimos los parametros de la barra de progresso
        $("#progressbar").progressbar({
            max: 200, // definimos el valor maximo
            value: valor, // definimos el valor actual
            complete: function () {
                // en el momento que se llene, detenemos el setInterval
                clearInterval(int);
            }
        });
        // evento que se ejecuta al pulsar el boton
        $(".button").click(function () {
            valor = 0;
            $(".button").button({
                disabled: true
            });
            // iniciamos el proceso nuevamente
            var int = setInterval(aumentar, 50);
        });

        function aumentar() {
            valor++;
            if (valor > 199) {
                to_right();
                valor = 0;
            }
            // Modificamos el valor de la variable value del progressbar
            $("#progressbar").progressbar("value", valor);
        }
        // indicamos que cada 50 milisegundos ejecute la funcion aumentar
        var int = setInterval(aumentar, 50);
    });
});
