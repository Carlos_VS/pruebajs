(function ($) {
    $.fn.mySlider = function () {
        var that = $(this);
        var childs = that.find("li");
        var collection = [];
        var position = 0;
        var timeoutId;
        var valor = 0;

        init();


        //**********CREATE ARROWS, FIRST BACKGROUND, TIME BAR AND NAVIGATOR*******
        function init() {
            $('#mycursor').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");

            that.hide();
            childs.each(function (i, that) {
                collection.push($(that).find("img").attr("src"));
            });
            var num_pic = childs.length;
            var ulAmazingSlider = that.parent();
            ulAmazingSlider.css("background-image", 'url(' + collection[0] + ')');

            //Add the progressbar, QR, and Arrows
            ulAmazingSlider.append("<div id='QR'></div><div id='progressbar'></div> <div class='arrow'><span id='left'><</span><span id='right'>></span></div><p class='nav'></p>");
            $("#QR").css("background-image", 'url("http://cdnqrcgde.s3-eu-west-1.amazonaws.com/wp-content/uploads/2013/11/jpeg.jpg" )');

            for (var i = 0; i < collection.length; i++) {
                $(".nav").append("<span class='circle' id=" + i + ">····</span>");
            }
            $("#left").fadeTo(0, 0.5);
            $("#right").fadeTo(0, 0.5);
            $('#' + position).css("background", "white");
            $('#' + position).css("color", "white");
        }


        //CHANGE IMAGE TO RIGHT OR LEFT BY ARROWS
        function change_img(direction) {
            var old_position = position;
            //********RIGHT**************
            if (direction == "right") {
                if (position >= collection.length - 1) {
                    position = 0;
                } else {
                    position = (position % collection.length) + 1;
                }
                valor = 0;
                that.parent().css("background-image", 'url(' + collection[position] + ')');
            }

            //********LEFT*****************
            if (direction == "left") {
                if (position < 1) {
                    position = collection.length - 1;
                } else {
                    position = (position % collection.length) - 1;
                }
                valor = 0;
                that.parent().css("background-image", 'url(' + collection[position] + ')');
            }
            refresh_navigator(position, old_position);
        }


        $('body').mouseenter(function () {
            $('#mycursor').show();

        });
        $('body').mousemove(function (e) {
            $('#mycursor').css('left', e.clientX - 20).css('top', e.clientY + 7);
        });

        //***********TO RIGHT***************
        function to_right() {
            that.parent().toggle("slide", {
                direction: 'left',
                duration: 400
            });

            setTimeout(function () {
                change_img("right");
                that.parent().toggle("slide", {
                    direction: 'right'
                });

            }, 400);
        }

        //*************TO LEFT**********
        function to_left() {
            that.parent().toggle("slide", {
                direction: 'right',
                duration: 400
            });

            setTimeout(function () {
                change_img("left");
                that.parent().toggle("slide", {
                    direction: 'left'
                });

            }, 400);
        }



        //***********JQUERYs MOUSE LEAVE************
        $("#right").mouseleave(function () {
            $("#right").fadeTo(0, 0.5);
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");

        });

        $("#left").mouseleave(function () {
            $("#left").fadeTo(0, 0.5);
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");
        });

        $(".circle").mouseleave(function () {
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");
        })


        //********HOVER NAVIGATOR*************
        $(".circle").hover(function () {
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");
            if (!timeoutId) {
                var num = $(this).attr('id');
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    navigator(num);
                    refresh_navigator(num, position);
                    $(this).css("background", "white");
                    $(this).css("color", "black");
                }, 500);
            }
        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }

        });

        //********HOVER RIGHT*******
        $("#right").hover(function () {
            $("#right").fadeTo(0, 1);
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");

            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    to_right();

                }, 500);
            }

        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
        });

        //*************HOVER LEFT*******
        $("#left").hover(function () {

            $("#left").fadeTo(0, 1);
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");

            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    to_left();

                }, 500);
            }

        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
        });


        //******AUXILARY FUNCTIONS********
        function to_number(number) {
            that.parent().css("background-image", 'url(' + collection[number] + ')');
            position = number;
            valor = 0;
        }

        function navigator(number) {
            that.parent().toggle("slide", {
                direction: 'left',
                duration: 400
            });
            setTimeout(function () {
                to_number(number)
                that.parent().toggle("slide", {
                    direction: 'right'
                });
            }, 400);
        }


        $(function mover_bar() {
            // definimos los parametros de la barra de progresso
            $("#progressbar").progressbar({
                max: 100, // definimos el valor maximo
                value: valor, // definimos el valor actual
                complete: function () {
                    // en el momento que se llene, detenemos el setInterval
                    clearInterval(int);
                }
            });
            // evento que se ejecuta al pulsar el boton
            $(".button").click(function () {
                valor = 0;
                $(".button").button({
                    disabled: true
                });
                // iniciamos el proceso nuevamente
                var int = setInterval(aumentar, 50);
            });

            function aumentar() {
                valor++;
                if (valor > 99) {
                    to_right();
                    valor = 0;
                }
                // Modificamos el valor de la variable value del progressbar
                $("#progressbar").progressbar("value", valor);
            }
            // indicamos que cada 50 milisegundos ejecute la funcion aumentar
            var int = setInterval(aumentar, 50);
        });

        function refresh_navigator(pos, old_pos) {
            $('#' + old_pos).css("background", "black");
            $('#' + old_pos).css("color", "black");
            $('#' + pos).css("background", "white");
            $('#' + pos).css("color", "white");
        }
    };

}(jQuery));



$(document).ready(function () {
    $(".amazingSlider").mySlider();
});
