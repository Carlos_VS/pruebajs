(function ($) {
    $.fn.mySlider = function () {
        var that = $(this);
        var childs = that.find("li");
        var collection = [];
        var position = 0;
        var timeoutId;
        var valor = 0;
        var listQR = ["http://cdnqrcgde.s3-eu-west-1.amazonaws.com/wp-content/uploads/2013/11/jpeg.jpg", "http://church-development.com/sites/default/files/assets/images/blog/Brianhunter_qr.jpg", "http://www.ekaterinawalter.com/wp-content/uploads/2013/11/qr_code_vote.jpg", "http://www.smartraveller.gov.au/images/download-smartraveller-qr.jpg"];

        var listDescriptions = ["<ul><li><strong>Descripción:</strong> piso en Puerta de Hierro, 447.00 m2, 6 dormitorios, 4 baños, exterior, orientación norte, 3 plazas de garaje </li><li><strong>Precio:  <span style='font-size:larger'>800.000€</span> </strong></li></ul>", "<ul ><li><strong>Descripción:</strong> expectacular piso en la finca más cotizada del barrio. Terraza, 7 habitaciones, 5 baños. Muy exclusivo </li><li><strong>Precio:  <span style='font-size:larger'>1.500.000€</span> </strong></li></ul>", "<ul ><li><strong>Descripción:</strong> piso ideal para una pareja. 1 baño, 3 habitaciones y un pequeño balcón. Está cerca de estación de metro y autobús, además de estar colindante al Madrid río </li><li><strong>Precio:  <span style='font-size:larger'>300.000€</span> </strong></li></ul>", "<ul ><li><strong>Descripción:</strong> San Francisco de Sales. Piso para reformar con muchas posibilidades. Todo el exterior ajardinado. Muy luminoso. Tiene plpaza de garage y trastero </li><li><strong>Precio:  <span style='font-size:larger'>700.000€</span> </strong></li></ul>"];

        init();

        //**********CREATE ARROWS, FIRST BACKGROUND, TIME BAR AND NAVIGATOR*******
        function init() {
            var num_pic = childs.length;
            inicializeCursor();
            addAllElements();
        }

        //CHANGE IMAGE TO RIGHT OR LEFT BY ARROWS
        function change_img_to(direction) {
            var old_position = position;
            //********RIGHT**************
            if (direction == "right") {
                if (position >= collection.length - 1) {
                    position = 0;
                } else {
                    position = (position % collection.length) + 1;
                }
            }

            //********LEFT*****************
            if (direction == "left") {
                if (position < 1) {
                    position = collection.length - 1;
                } else {
                    position = (position % collection.length) - 1;
                }
            }
            refresh_QR_and_BK_and_desc();
            refresh_navigator(position, old_position);
        }



        $('body').mouseenter(function () {
            $('#mycursor').show();
        });

        $('body').mousemove(function (e) {
            $('#mycursor').css('left', e.clientX - 20).css('top', e.clientY + 7);
        });

        //***********TO RIGHT***************
        function to_right() {
            that.parent().toggle("slide", {
                direction: 'left',
                duration: 400
            });

            setTimeout(function () {
                change_img_to("right");
                that.parent().toggle("slide", {
                    direction: 'right'
                });

            }, 400);
        }

        //*************TO LEFT**********
        function to_left() {
            that.parent().toggle("slide", {
                direction: 'right',
                duration: 400
            });

            setTimeout(function () {
                change_img_to("left");
                that.parent().toggle("slide", {
                    direction: 'left'
                });

            }, 400);
        }



        //***********JQUERYs MOUSE LEAVE************
        $("#right").mouseleave(function () {
            $("#right").fadeTo(0, 0.5);
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");

        });

        $("#left").mouseleave(function () {
            $("#left").fadeTo(0, 0.5);
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");
        });

        $(".circle").mouseleave(function () {
            $('#mycursor.hover').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");
        })

        //********HOVER NAVIGATOR*************
        $(".circle").hover(function () {
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");

            if (!timeoutId) {
                var num = $(this).children().attr('id');
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    navigator(num);
                    refresh_navigator(num, position);
                    $(this).css("background", "white");
                    $(this).css("color", "black");
                }, 500);
            }
        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }

        });

        //********HOVER RIGHT*******
        $("#right").hover(function () {
            $("#right").fadeTo(0, 1);
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");

            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    to_right();

                }, 500);
            }

        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
        });

        //*************HOVER LEFT*******
        $("#left").hover(function () {
            $("#left").fadeTo(0, 1);
            $('#mycursor').addClass('hover');
            $('#mycursor.hover').css("background-image", "url('https://cdn2.iconfinder.com/data/icons/seo-web-optomization-ultimate-set/512/time_efficiency-512.png')");

            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    to_left();

                }, 500);
            }

        }, function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
        });


        //******AUXILARY FUNCTIONS********
        function to_number(number) {
            that.parent().css("background-image", 'url(' + collection[number] + ')');
            $("#QR").css("background-image", "url('" + listQR[number] + "')");
            $("#description").html(listDescriptions[number]);

            position = number;
            valor = 0;
        }

        function navigator(number) {
            that.parent().toggle("slide", {
                direction: 'left',
                duration: 400
            });
            setTimeout(function () {
                to_number(number)
                that.parent().toggle("slide", {
                    direction: 'right'
                });
            }, 400);
        }

        function inicializeCursor() {
            $('#mycursor').css("background-image", "url('http://icons.iconarchive.com/icons/icons8/ios7/256/Hands-Whole-Hand-icon.png')");
            that.hide();
        }

        function addAllElements() {
            var ulAmazingSlider = that.parent();

            childs.each(function (i, that) {
                collection.push($(that).find("img").attr("src"));
            });
            ulAmazingSlider.css("background-image", 'url(' + collection[0] + ')');

            //Add the progressbar, QR, and Arrows
            ulAmazingSlider.append("<div id='QR'></div><div id='progressbar'></div> <div class='arrow'><span id='left'><</span><span id='right'>></span></div><div class='nav'></div><div id='prizeAndDescription'><div id='logo'></div><div id='description'></div></div>");
            $("#QR").css("background-image", "url('" + listQR[0] + "')");
            $("#logo").css("background-image", "url('http://www.gilmar.es/img/gilmar_logo.jpg')");
            $(".arrow").css("color", "white");

            for (var i = 0; i < collection.length; i++) {
                $(".nav").append("<div class='circle' ><div id=" + i + " class='contentCircle'>····</div></div>");
                $("#left").fadeTo(0, 0.5);
                $("#right").fadeTo(0, 0.5);
                $('#' + position).css("background", "white");
                $('#' + position).css("color", "white");
            }
            $("#description").append(listDescriptions[0])
        }

        function refresh_QR_and_BK_and_desc() {
            valor = 0;
            that.parent().css("background-image", 'url(' + collection[position] + ')');
            $("#QR").css("background-image", "url('" + listQR[position] + "')");
            $("#description").html(listDescriptions[position]);
        }


        $(function mover_bar() {
            $("#progressbar").progressbar({
                max: 100,
                value: valor,
                complete: function () {
                    clearInterval(int);
                }
            });

            function aumentar() {
                valor++;
                if (valor > 99) {
                    to_right();
                    valor = 0;
                }
                // Modificamos el valor de la variable value del progressbar
                $("#progressbar").progressbar("value", valor);
            }
            // indicamos que cada 50 milisegundos ejecute la funcion aumentar
            var int = setInterval(aumentar, 50);
        });

        function refresh_navigator(pos, old_pos) {
            $('#' + old_pos).css("background", "black");
            $('#' + old_pos).css("color", "black");
            $('#' + pos).css("background", "white");
            $('#' + pos).css("color", "white");
        }
    };
}(jQuery));



$(document).ready(function () {
    $(".amazingSlider").mySlider();
});
