var fondos = ["http://2.bp.blogspot.com/_EZ16vWYvHHg/S-Bl2fuyyWI/AAAAAAAAMKc/DNayYJK8mEo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-20.jpg", "http://www.enbolivia.com/files/imagen_corporativa.jpg", "http://2.bp.blogspot.com/_EZ16vWYvHHg/S-BkIUOuxeI/AAAAAAAAMH0/xJllGDfubHo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-3.jpg", "http://www.imagenes-juegos.com/halo-4-imagen-i306424-i.jpg", "http://img.emol.com/2012/01/25/impresionante-imagen-tierra_14302.jpg"];
var pos = 0;
var valor = 0;
var int = 0;

function aux() {
    console.log("hola");
}

function change_Img(direction) {
    //******DERECHA*******
    if (direction === "right") {
        if (pos >= fondos.length - 1) {
            pos = 0;
        } else {
            pos = (pos % fondos.length) + 1;
        }
        valor = 0;
        $("#background").css('background-image', 'url(' + fondos[pos] + ')').fadeIn(200);
    }

    //*****IZQUIERDA*****
        if (direction === "left") {
            if (pos < 1) {
                pos = fondos.length - 1;
            } else {
                pos = (pos % fondos.length) - 1;
            }
            valor = 0;
            $("#background").css('background-image', 'url(' + fondos[pos] + ')').fadeIn(200);
        }
}


$(document).ready(function () {
    $(".img-responsive").fadeTo(0, 0.5);
    $("#right").mouseleave(function () {
        $("#right").fadeTo(0, 0.5);
    });

    $("#left").mouseleave(function () {
        $("#left").fadeTo(0, 0.5);
    });


    $("#right").mouseenter(function () {
        $("#right").fadeTo(0, 1);
        $("#background").fadeOut(200);
        $("#background").css('background-image', 'none');
        $("#background").css('background-color', 'black');
        setTimeout(change_Img, 200, "right");
    });

    $("#left").mouseenter(function () {
        $("#left").fadeTo(0, 1);
        $("#background").fadeOut(200);
        $("#background").css('background-image', 'none');
        $("#background").css('background-color', 'black');
        setTimeout(change_Img, 200, "left");
    });



    $(function mover() {

        // definimos los parametros de la barra de progresso
        $("#progressbar").progressbar({
            max: 200, // definimos el valor maximo
            value: valor, // definimos el valor actual
            complete: function () {
                // en el momento que se llene, detenemos el setInterval
                clearInterval(int);
            }
        });
        // evento que se ejecuta al pulsar el boton
        $(".button").click(function () {
            valor = 0;
            $(".button").button({
                disabled: true
            });
            // iniciamos el proceso nuevamente
            var int = setInterval(aumentar, 50);
        });

        function aumentar() {
            valor++;
            if (valor > 199) {
                $("#background").fadeOut(200);

                change_Img("right");
                //console.log("FIN");
                valor = 0;
            }
            // Modificamos el valor de la variable value del progressbar
            $("#progressbar").progressbar("value", valor);
        }
        // indicamos que cada 50 milisegundos ejecute la funcion aumentar
        var int = setInterval(aumentar, 50);

    });


});
